//
// Created by Juan Bernardo Gómez Mendoza on 11/9/23.
//

#include <pthread.h>
#include <unistd.h>
#include <stdio.h>

typedef struct { // Para intercambio de información
    int valor;   // entre hilos.
    pthread_mutex_t *mutex;
} Parametros_t;

void* produceTask(void* param) {
    Parametros_t* datos = param;
    while(1) {
        pthread_mutex_lock(datos->mutex);  // Para hasta poder
        datos->valor++;     // tener acceso al mutex.
        usleep(250000);
        printf("\x1B[32m-> Dato %d producido.\n\r \x1B[0m",
                datos->valor);
        pthread_mutex_unlock(datos->mutex);  // Libera.
        usleep(100000);
    }
}
void* consumeTask(void* param) {
    Parametros_t* datos = param;
    while(1) {
        if (pthread_mutex_trylock(datos->mutex)) {
            printf("\x1B[35m<- Dato %d consumido.\n\r \x1B[0m",
                   datos->valor);
            usleep(330000);
            pthread_mutex_unlock(datos->mutex);
            usleep(100000);
        } else {
            printf("\x1B[31m[E] Falló el consumo. "
                   " Reintentando en 50ms.\n\r\x1B[0m");
            usleep(50000);
        }
    }
}

int main(int argc, char* argv[]) {
    pthread_mutex_t mutex;
    pthread_mutex_init(&mutex, NULL);
    Parametros_t compartido = { 0, &mutex };
    pthread_t productor, consumidor;
    pthread_create(&productor, NULL,
                   produceTask,(void*)&compartido);
    pthread_create(&consumidor, NULL,
                   consumeTask,(void*)&compartido);
    pthread_join(consumidor, NULL);
    pthread_join(productor, NULL);
    pthread_mutex_destroy(&mutex);
    return 0;
}
